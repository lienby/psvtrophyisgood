# Releases

# v1.2
+ Added PSN Stealer - You can now copy timestamps from PSN! (probably patched now lol)
+ Fixed a bug with the lock function
+ Fixed Issue #3 where time goes -1

Windows x86-setup: 
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-win32-setup-1.2.exe  
Windows x86-zip:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-win32-1.2.zip  
Ubuntu x64:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-ubuntu64-1.2.tar.gz  

# v1.1
+ Fixed a bug where clicking unlock all while you already had some unlocked w stamps would cause it not to sync
+ The "Change Timestamp" feature will now remember the last time you used.
+ The Random and Random all buttons will now ask you to choose a start and end date.
+ Fixed a bug where the progress bar wouldn't show up properly on the vita itself
+ Fixed a bug where in some time zones setting a timestamp to 2000/1/1 would display as NaN

Windows x86-setup:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-win32-setup-1.1.exe  
Windows x86-zip:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-win32-1.1.zip  
Ubuntu x64:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-ubuntu64-1.1.tar.gz  

# v1.0
Windows x86-setup:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-win32-setup-1.0.exe  
Windows x86-zip:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-win32-1.0.zip  
Ubuntu x64:  
https://bitbucket.org/SilicaAndPina/psvtrophyisgood/downloads/psvtrophyisgood-ubuntu64-1.0.tar.gz  

# Readme

psvtrophyisgood Trophy Folder editor For PSVITA!  

using this tool you can edit NPWR folder data, this allows you to unlock, lock, and change the timestamp  
of any trophy in any set.  

# Importing trophys into PSVTIG:  
In order to import trophys into psvtig you need 2 things.  
1. the trophy conf folder, you can find this at ur0:user/00/trophy/conf.  
2. the decrypted trophy data folder, goto ur0:user/00/trophy/data,   
hover over the folder you want and press triangle > open decrypted in vitashell.  

then in PSVTIG click import trophy, and select the conf folder first, and then select the data folder.  
if everything is OK it should import into the program. from there you can edit it.  

# Exporting trophys from PSVTIG:  
So you made some edits to the trophy set, and now you want to put it back onto   
the vita to sync. well first on the main screen select the trophy you want and goto export set  
then just select where you want it, and you'll now have a data/NPWR and a conf/NPWR folder  

copy the data folder somewhere on your vita (just not ur0:/user/00/trophy/data),  
then select both TRPTRANS and TRPTITLE in vitashell and goto copy  
repeat the steps for getting the decrypted files in vitashell again  
once your in the "open decrypted" thing press triangle and paste.   

your folders are now encrypted again so the vita will read them!  

if the trophy app doesnt update, delete ur0:user/00/trophy/data/sce_trop and ur0:user/00/trophy/db  
open the app again it should say restoring, the progress of the set will NOT be correct, do not worry  
let it sync and it will update to the correct progress,  

# Credits
Massive thanks to LaughTracks aka In_Her_Own_Words (found tons of bugs for me to fix :D)
Massive thanks to @motoharu @TheOfficialFloW and @dots-tb for trophy decryption/encryption  
Thanks to AnalogMan for helping me with timezones.  
and thanks to the following beta testers:
frosty, wosley, shadow, levi, Justin, and LaughTracks aka In_Her_Own_Words (found tons of bugs for me to fix :D)

Please no commercial use / trophy service. unless its a free trophy service thanks.


Want to donate to me? really? ok: https://goo.gl/dvHkEh
